
# Example chaincode use (bcc-client-cli)
  
-  [Motivation](#Motivation)
-  [Introduction](#Introduction)
-  [Getting Started](#Getting-Started)

## Motivation


_bcc-client-cli_ provides documentation and a tool to get started creating and executing stories using the skuchain blockchain


## Getting Started
 
##### Installation

``` 
$> git clone git@bitbucket.org:skuchaindev/bcc-client-cli.git
$> npm install
```

##### Running the examples

1. Set your network parameters
 - SERVER
 - NETWORK NAME


```
$> export SERVER_URL=<SERVER_URL>; export NETWORK=<NETWORK_NAME>;
```
 2. Run a story, for more information see [documentation](doc/scenarios)

 3. Now visit the [output](stories/output/) directory to see the result

## Getting started

The [doc](doc/scenarios) folder contains documentation on how to create and run your own stories

### Creating your own story

 - [Alice transfers 10L of milk to Bob](doc/scenarios/transferunit.md)
 - [Alice adds event information to an operation](doc/scenarios/event.md)
 - [Alice transfers 10L of milk to warehouse](doc/scenarios/addtocontainer.md)
 - [Ship takes 10L of milk from the warehouse](doc/scenarios/removefromcontainer.md)
 - [Verify any transaction](doc/verify.md)