const vorpal = require('vorpal')();
const fs = require('fs');
const { argv } = require('yargs')
  .version('0.03')
  .option('storybase', {
    describe: 'the story file to use',
  })
  .option('storyfile', {
    describe: 'the run file to use',
  });
const { serverURL, networkName } = require('./lib/constants.js');

let story = {
  delimiter: 'bcc ~/a-new-story $ ',
  server: {
    tpware: `https://${serverURL}/bcctuxedopopsware`,
    kvware: `https://${serverURL}/bcc-kv-ware`,
    dgraphApiUrl: `https://${serverURL}/dgraphio`,
  },
  users: [],
  popcodes: [],
  skupops: [],
  callstack: [],
  txlist: [],
};

const exec = async ({ cmd, data }) => {
  return new Promise(res => {
    res(vorpal.execSync(cmd, data));
  });
};

const dorun = async run => {
  const keys = Object.keys(run);
  for (let i = 0; i < keys.length; i += 1) {
    const element = run[i];
    console.log('doing..', element.cmd);
    // eslint-disable-next-line no-await-in-loop
    const fcmd = vorpal.find(element.cmd);
    if (!fcmd) {
      require(`./lib/cmds/${element.cmd}.js`)(vorpal, story);
    }
    await exec(element);
    console.log('done..', element.cmd);
  }
};

if (fs.existsSync(argv.storybase)) {
  console.log('Loading story from...', argv.storybase);
  story = require(argv.storybase);
} else {
  console.log('Staring with a blank story');
}
if (fs.existsSync(argv.storyfile)) {
  console.log('Loading run from...', argv.storyfile);
  run = require(argv.storyfile);
  dorun(run);
} else {
  console.log('No run file.. exiting');
  process.exit();
}
