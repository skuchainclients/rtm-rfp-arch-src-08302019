/* eslint-disable no-console */
// const bc3lib = require('bcc-client-lib');
// const axios = require('axios');
// const uuidv4 = require('uuid/v4');
// const getBalance = require('./balance');
const { queryByTxId } = require('./query');

const verify = async params => {
  const { server, txlist } = params;

  const result = [];
  for (let i = 0; i < txlist.length; i += 1) {
    const { txid } = txlist[i];
    const txId = txid;
    const queryParams = {
      txId,
      server,
    };
    const res = await queryByTxId(queryParams);
    result.push(res);
  }
  return result;
};

module.exports = async (params, cb) => {
  const mt = await verify(params);
  return cb(mt);
};
