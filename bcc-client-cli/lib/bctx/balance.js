const axios = require('axios');

module.exports = async (params, server) => {
  try {
    return await axios.get(`${server}/balance`, {
      params,
    });
  } catch (error) {
    console.error(error);
    return error;
  }
};
