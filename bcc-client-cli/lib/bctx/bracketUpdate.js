/* eslint-disable no-console */
const bc3lib = require('bcc-client-lib');
const axios = require('axios');
const uuid = require('uuid');

const bracketUpdate = async params => {
  const { server, Rules, Hooks, from, to, type, findWith } = params;
  const { kvware } = server;

  const doc = { PONumber: 'ANC123' };

  // Generate keys
  const keyPair = bc3lib.keys.createKeys();
  const key = bc3lib.keys.createProofKey(uuid.v4());

  // Create a bracket txn
  const txn = new bc3lib.tx.BracketTX();
  txn.key = key;

  // Add additional info for bracket
  txn.type = type;
  txn.from = from;
  txn.to = to;

  // Sign the transaction with the private key
  const { sig, dataHash } = bc3lib.tx.BracketTX.sign(
    key,
    doc,
    keyPair.privateKey,
  );
  txn.dataHash = dataHash;
  txn.sig = sig;
  txn.publicKey = keyPair.publicKey;

  const eventData = new bc3lib.bracket.BracketEvent({
    tx: txn,
    doc,
    findWith,
  });

  const createProof = async () => {
    const createPayload = new bc3lib.payload.CreateProofPL();
    const createPL = createPayload.validateAndFill({
      Name: key,
      Threshold: 1,
      PubKeys: [keyPair.publicKey],
      EventData: JSON.stringify(eventData),
      Rules,
      Hooks,
    });
    try {
      const createRes = await axios.post(`${kvware}/v1/create`, createPL);
      return createRes.data;
    } catch (er) {
      console.log('------------Create Error ', er);
    }
    return null;
  };

  const signProof = async () => {
    const signPayload = new bc3lib.payload.SignProofPL();
    const signPL = signPayload.validateAndFill({
      Name: key,
      Data: dataHash,
      Signatures: [sig],
      EventData: JSON.stringify(eventData),
    });

    try {
      const signRes = await axios.post(`${kvware}/v1/sign`, signPL);
      return signRes.data;
    } catch (erw) {
      console.log('------------Error ', erw);
    }
    return null;
  };

  const createData = await createProof();
  await signProof();
  return createData;
};

module.exports = async (params, cb) => {
  const bc = await bracketUpdate(params);
  return cb(bc);
};
