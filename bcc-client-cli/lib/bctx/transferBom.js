/* eslint-disable no-console */
const axios = require('axios');
const bc3lib = require('bcc-client-lib');
const uuidv4 = require('uuid/v4');
const getBalance = require('./balance');

const transferBom = async params => {
  const {
    destPopcode,
    DestAmount,
    Hooks,
    Rules,
    SourceAmount,
    sourcePopcode,
    server,
    AuthenticatedServerMode,
  } = params;
  const { tpware } = server;
  const { keys: destKeys } = destPopcode;
  const { address: DestPopcodeAddress } = destKeys;
  const { keys: sourceKeys } = sourcePopcode;
  const { address: SourcePopcodeAddress } = sourceKeys;

  let counter;
  let balance;
  if (AuthenticatedServerMode) {
    counter = uuidv4().replace(/-/g, '');
  } else {
    const popcodeAddress = SourcePopcodeAddress;
    balance = await getBalance({ popcodeAddress }, tpware);
    counter = balance.data.Counter;
  }

  // Get a TransferTX signature
  const sig = bc3lib.tx.TransferTX.sign({
    counter,
    sourcePopcodeAddress: sourceKeys.address,
    destPopcodeAddress: destKeys.address,
    Amount: SourceAmount,
    privateKey: sourceKeys.privateKey,
  });

  const tpParams = {
    AuthenticatedServerMode: false,
    DestAmount,
    DestData: '{}',
    DestEventData: '{}',
    DestPopcodeAddress,
    Hooks,
    PopcodePubKey: sourceKeys.publicKey,
    PopcodeSig: sig,
    Rules,
    SourceAmount,
    SourceCounter: counter,
    SourcePopcodeAddress,
    SourceEventData: '{}',
    SourceData: '{}',
    Version: 1.1,
  };

  try {
    return await axios.post(`${tpware}/transferBom`, tpParams);
  } catch (error) {
    console.error(error);
    return error;
  }
};

module.exports = async (params, cb) => {
  const to = await transferBom(params);
  return cb(to.data);
};
