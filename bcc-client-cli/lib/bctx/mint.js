/* eslint-disable no-console */
const bc3lib = require('bcc-client-lib');
const axios = require('axios');
const uuidv4 = require('uuid/v4');
const getBalance = require('./balance');

const mint = async params => {
  // Get balance
  const {
    Asset,
    Data,
    EventData,
    Hooks,
    popcode,
    Rules,
    server,
    AuthenticatedServerMode,
  } = params;
  const { keys } = popcode;
  const {
    address: popcodeAddress,
    privateKey,
    publicKey: CreatorPubKey,
  } = keys;

  const { tpware } = server;

  let counter;
  let balance;
  if (AuthenticatedServerMode) {
    counter = uuidv4().replace(/-/g, '');
  } else {
    balance = await getBalance({ popcodeAddress }, tpware);
    counter = balance.data.Counter;
  }

  const PopcodeAddress = popcodeAddress;

  const pc = {
    PopcodeAddress,
    Asset,
    Data,
    CreatorPubKey,
  };

  // Get a mintTX signature
  const sig = bc3lib.tx.MintTX.sign({
    Counter: counter,
    popcode: pc,
    privateKey,
  });
  pc.CreatorSig = sig;

  const Popcode = {
    Asset,
    CreatorPubKey,
    CreatorSig: sig,
    Counter: counter,
    Data,
    EventData,
    PopcodeAddress,
  };

  const popcodeKeys = Object.keys(Popcode);
  const xtraKeys = Object.keys(params.xtra);

  xtraKeys.forEach(item => {
    if (!popcodeKeys.includes(item)) {
      Popcode[item] = params.xtra[item];
    }
  });

  // Validate and fill out the mint payload
  const mintPL = {
    Popcode,
    Rules,
    Hooks,
  };

  if (AuthenticatedServerMode) {
    mintPL.AuthenticatedServerMode = true;
  }

  try {
    return await axios.post(`${tpware}/mint`, mintPL);
  } catch (error) {
    console.error(error);
    return error;
  }
};

module.exports = async (params, cb) => {
  const mt = await mint(params);
  return cb(mt.data);
};
