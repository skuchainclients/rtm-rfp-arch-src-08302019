/* eslint-disable no-console */
const axios = require('axios');

const adjustPopcode = async params => {
  /*
  const { Asset, Data, EventData, Hooks, popcode, Rules, server } = params;
  const { keys } = popcode;
  const {
    address: popcodeAddress,
  } = keys;
  */
  const { Asset, Data, EventData, Hooks, popcode, Rules, server } = params;
  const { keys } = popcode;
  const { address: popcodeAddress, BOMArray: BOM } = keys;
  const { tpware } = server;

  const PopcodeAddress = popcodeAddress;

  // Validate and fill out the adjust popcode payload
  const adjustPopcodePL = {
    Popcode: {
      Asset,
      Data,
      EventData,
      PopcodeAddress,
    },
    Rules,
    Hooks,
  };
  if (adjustPopcodePL.Popcode.Asset.Amount) {
    adjustPopcodePL.HasAmount = true;
  }
  if (adjustPopcodePL.Popcode.Asset.UnitOfMeasure) {
    adjustPopcodePL.HasUnitofmeasure = true;
  }
  if (adjustPopcodePL.Popcode.Asset.Name) {
    adjustPopcodePL.HasName = true;
  }

  if (BOM) {
    adjustPopcodePL.Popcode.BOM = BOM;
    adjustPopcodePL.HasBom = true;
  }

  try {
    return await axios.post(`${tpware}/adjustPopcode`, adjustPopcodePL);
  } catch (error) {
    console.error(error);
    return error;
  }
};

module.exports = async (params, cb) => {
  const mt = await adjustPopcode(params);
  return cb(mt.data);
};
