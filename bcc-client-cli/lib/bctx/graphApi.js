const { queryByTxId, queryByAddress } = require('./query');

module.exports = {
  byTxId: async (params, cb) => {
    const mt = await queryByTxId(params);
    return cb(mt);
  },
  byAddress: async (params, cb) => {
    const mt = await queryByAddress(params);
    return cb(mt);
  },
};
