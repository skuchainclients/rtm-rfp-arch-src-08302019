/* eslint-disable no-console */
const bc3lib = require('bcc-client-lib');
const axios = require('axios');
const getBalance = require('./balance');

const blobCreate = async params => {
  const { Data, EventData, Hooks, popcode, Rules, server } = params;
  const { keys } = popcode;
  const {
    address: popcodeAddress,
    privateKey,
    publicKey: CreatorPubKey,
  } = keys;
  const { tpware } = server;

  // Get balance
  const balance = await getBalance({ popcodeAddress }, tpware);
  const PopcodeAddress = popcodeAddress;

  // Get a BlobTX signature
  const sig = bc3lib.tx.BlobTX.sign({
    Counter: balance.data.Counter,
    PopcodeAddress,
    Data,
    privateKey,
  });

  const tpParams = {
    PopcodeAddress,
    Counter: balance.data.Counter,
    CreatorPubKey,
    CreatorSig: sig,
    Data,
    EventData,
    Rules,
    Hooks,
  };
  try {
    return await axios.post(`${tpware}/blobCreate`, tpParams);
  } catch (error) {
    console.error(error);
    return error;
  }
};

module.exports = async (params, cb) => {
  const bc = await blobCreate(params);
  return cb(bc.data);
};
