const { post } = require('axios');

module.exports.queryByTxId = async params => {
  const { txId, server } = params;
  const { dgraphApiUrl } = server;
  const query = `
    query popcodes($txId: string) {
      popcode(func: eq(popcode.base.txid, $txId)) {
        expand(_all_)
      }
    }
`;
  try {
    const response = await post(`${dgraphApiUrl}/query`, {
      query,
      vars: { $txId: txId },
    });
    return response.data;
  } catch (er) {
    console.log(er.message);
  }
  return null;
};

module.exports.queryByAddress = async params => {
  const { address, server } = params;
  const { dgraphApiUrl } = server;
  const query = `
    query popcodes($address: string) {
      popcode(func: eq(popcode.base.pcaddress, $address), orderasc: popcode.base.timestamp) {
        uid
        popcode.base.pcaddress
        popcode.base.asset
        popcode.base.data
        popcode.base.timestamp
        popcode.base.counter
        popcode.base.prevCounter
        popocde.base.parent
        popcode.metadata.spot
        popcode.base.tx {
          popcode.tx.txid
          popcode.tx.txtype
        }
      }
    }
`;
  try {
    const response = await post(`${dgraphApiUrl}/query`, {
      query,
      vars: { $address: address },
    });
    return response.data;
  } catch (er) {
    console.log(er.message);
  }

  return null;
};
