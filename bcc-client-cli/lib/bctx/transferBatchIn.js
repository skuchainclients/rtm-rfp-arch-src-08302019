/* eslint-disable no-console */
const axios = require('axios');
const bc3lib = require('bcc-client-lib');
const uuidv4 = require('uuid/v4');
const getBalance = require('./balance');

const transferBatchIn = async params => {
  const {
    Amount,
    AuthenticatedServerMode,
    SourceData,
    SourceEventData,
    DestData,
    DestEventData,
    destPopcode,
    Hooks,
    sourcePopcode,
    Rules,
    server,
    sourceBomPopcode,
  } = params;
  const { keys: destKeys } = destPopcode;
  const { address: DestPopcodeAddress } = destKeys;
  const { keys: sourceKeys } = sourcePopcode;
  const { address: SourcePopcodeAddress } = sourceKeys;
  const { keys: sourceBomKeys } = sourceBomPopcode;
  const { address: SourcePopcodeBomAddress } = sourceBomKeys;
  const { tpware } = server;

  let counter;
  let balance;
  if (AuthenticatedServerMode) {
    counter = uuidv4().replace(/-/g, '');
  } else {
    const popcodeAddress = SourcePopcodeAddress;
    balance = await getBalance({ popcodeAddress }, tpware);
    counter = balance.data.Counter;
  }

  // Get a TransferUnitTX signature
  const sig = bc3lib.tx.TransferTX.sign({
    counter,
    sourcePopcodeAddress: sourceKeys.address,
    destPopcodeAddress: destKeys.address,
    Amount,
    privateKey: sourceKeys.privateKey,
  });

  const tpParams = {
    Amount,
    AuthenticatedServerMode: false,
    DestData,
    DestEventData,
    DestPopcodeAddress,
    EventData: '{}',
    Hooks,
    PopcodePubKey: sourceKeys.publicKey,
    PopcodeSig: sig,
    Rules,
    SourceCounter: counter,
    SourceData,
    SourceEventData,
    SourcePopcodeAddress,
    SourcePopcodeBomAddress,
    Version: 1.1,
  };

  try {
    return await axios.post(`${tpware}/transferBatchIn`, tpParams);
  } catch (error) {
    console.error(error);
    return error;
  }
};

module.exports = async (params, cb) => {
  const bu = await transferBatchIn(params);
  return cb(bu.data);
};
