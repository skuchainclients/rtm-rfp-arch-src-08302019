const _ = require('lodash');
const adjustPopcode = require('../bctx/adjustPopcode');
const { networkName } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  popcode: {
    keys: {},
  },
  data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:adjustPopcode` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

const getPopcode = (tx = [], skuid) => {
  const currentTx = tx.find(t => {
    if (_.has(t, ['txparams', 'skuid'])) {
      const getSkuid = _.get(t, ['txparams', 'skuid']);
      return getSkuid === skuid;
    }
    return null;
  });
  if (currentTx) {
    return _.get(currentTx, ['txparams', 'popcode', 'keys']);
  }
  return null;
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.adjustPopcode',
  };

  // eslint-disable-next-line func-names
  vorpal
    .command(cmd.name, 'adjusts a popcode')
    .action(async function(args, cb) {
      const { users = [], server, txlist } = story;
      const self = this;

      const execCodes = async result => {
        self.log(`Adjusting a popcode`);
        const k = result;

        if (!result.amount && !result.uom && !result.bom && !result.name) {
          throw Error('None of the amount/uom/bom/name fields are set!');
        }

        const currentCreator = users.find(s => s.name === result.creator);
        if (currentCreator) {
          params.creator = currentCreator;
        }
        params.server = server;

        if (args.data) {
          params.Data = JSON.stringify(args.data);
        } else {
          params.Data = JSON.stringify({ hello: 'world' });
        }

        if (args.eventdata) {
          params.EventData = JSON.stringify(args.eventdata);
        } else {
          params.EventData = JSON.stringify({});
        }

        params.Asset = {};
        if (result.amount) {
          params.Asset.Amount = parseFloat(result.amount);
        }
        if (result.name) {
          params.Asset.Name = result.name;
        }
        if (result.uom) {
          params.Asset.UnitOfMeasure = result.uom;
        }
        params.skuid = result.skuid;

        const popcode = getPopcode(txlist, result.skuid);
        if (!popcode) {
          throw Error('popcode not found!');
        }
        if (result.bom) {
          popcode.BOMArray = result.bom;
        }
        params.popcode.keys = popcode;

        params.Rules = [{ Name: `*-${networkName}:adjustPopcode` }];
        params.Hooks = [{ Name: `*-${networkName}` }];

        Object.assign(k, params);
        const tx = {
          txtype: 'adjustPopcode',
          txparams: _.cloneDeep(params),
        };

        // cb()
        // need to adjustPopcode here
        await adjustPopcode(params, ({ txId }) => {
          // add result info to the right spot in story
          tx.txid = txId;
          txlist.push(tx);
          return cb ? cb(story) : null;
        });
      };

      if (args.creator) {
        await execCodes(args);
      } else {
        console.log('Please use the following guide to input the criteria');
        console.log(
          'John (creator) is adjusting popcode, a 100 (amount) kgs (uom) of sugar (name) for his SKU 1234 (skuid)',
        );
        this.prompt(
          [
            {
              type: 'input',
              name: 'creator',
              message: 'Creator ? ',
            },
            {
              type: 'input',
              name: 'amount',
              message: 'amount: How many ? ',
            },
            {
              type: 'input',
              name: 'uom',
              message: 'uom: kgs / lbs ... ? ',
            },
            {
              type: 'input',
              name: 'name',
              message: 'name: seeds / widgets ... ? ',
            },
            {
              type: 'input',
              name: 'skuid',
              message: 'skuid: something unique ... ? ',
            },
          ],
          async function(result) {
            await execCodes(result, cb);
          },
        );
      }
    });
};
