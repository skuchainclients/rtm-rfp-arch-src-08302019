const _ = require('lodash');
const blobCreate = require('../bctx/blobCreate');
const { networkName } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  popcode: {
    keys: {},
  },
  Data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:updatePopcode` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.event',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'event a popcode').action(async function(args, cb) {
    const { server, txlist } = story;
    const execCodes = async result => {
      const k = result;
      params.server = server;

      if (args.eventdata) {
        params.EventData = JSON.stringify(args.eventdata);
      } else {
        params.EventData = JSON.stringify({});
      }

      params.Asset = {
        Skuid: result.skuid,
      };
      params.Rules = [{ Name: `*-${networkName}:updatePopcode` }];
      params.Hooks = [{ Name: `*-${networkName}` }];
      const findPopcode = txlist.find(txItem => {
        const { txparams } = txItem;
        const { skuid } = txparams || {};
        return skuid === result.skuid;
      });

      if (!findPopcode) {
        throw Error('Popcode not found!');
      }

      params.popcode.keys = findPopcode.txparams.popcode.keys;
      params.Data = JSON.stringify({ address: params.popcode.keys.address });

      if (findPopcode && findPopcode.txparams && findPopcode.txparams.creator) {
        params.creator =
          findPopcode && findPopcode.txparams && findPopcode.txparams.creator;
      }
      Object.assign(k, params);
      const tx = {
        txtype: 'updatePopcode',
        txparams: _.cloneDeep(params),
      };

      await blobCreate(params, ({ txId }) => {
        // add result info to the right spot in story
        tx.txid = txId;
        txlist.push(tx);
        return cb ? cb(story) : null;
      });
    };

    if (args.skuid) {
      await execCodes(args);
    } else {
      this.prompt(
        [
          {
            type: 'input',
            name: 'skuid',
            message: 'skuid: something unique ... ? ',
          },
        ],
        async result => {
          await execCodes(result, cb);
        },
      );
    }
  });
};
