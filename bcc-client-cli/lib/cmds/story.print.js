module.exports = (vorpal, story) => {
  const cmd = {
    name: `story.print`,
  };
  vorpal.command(cmd.name, 'prints a story').action((args, cb) => {
    this.log(JSON.stringify(story, null, 2));
    cb();
  });
};
