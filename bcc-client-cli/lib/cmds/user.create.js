const bc3lib = require('bcc-client-lib');

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'user.create',
  };

  vorpal.command(cmd.name, 'loads a story').action((args, cb) => {
    const execCodes = result => {
      const { name } = result;
      story.users.push({
        name,
        keys: bc3lib.keys.createKeys(),
      });
      return cb ? cb(story) : null;
    };

    if (args.name) {
      execCodes(args);
    } else {
      this.prompt(
        {
          type: 'input',
          name: 'name',
          message: 'username/id?',
        },
        result => {
          execCodes(result);
        },
      );
    }
  });
};
