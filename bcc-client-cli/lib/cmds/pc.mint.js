const bc3lib = require('bcc-client-lib');
const _ = require('lodash');
const mint = require('../bctx/mint');
const { networkName, serverURL } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  popcode: {
    keys: {},
  },
  data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:mintPopcode` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.mint',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'mints a popcode').action(async (args, cb) => {
    const { users = [], server, popcodes, skupops, txlist } = story;

    const execCodes = async result => {
      console.log(`Minting a popcode: ${result.name}`);
      const k = result;

      const currentCreator = users.find(s => s.name === result.creator);
      if (currentCreator) {
        params.creator = currentCreator;
      }
      params.server = server;

      const keys = bc3lib.keys.createKeys();

      params.popcode.keys = currentCreator
        ? currentCreator.keys
        : bc3lib.keys.createKeys();
      params.popcode.keys.address = keys.address;
      popcodes.push(params.popcode.keys);

      if (args.eventdata) {
        params.Data = JSON.stringify(args.eventdata);
      } else {
        params.Data = JSON.stringify({ popcodeName: result.creator });
      }

      const skuid = {
        domain: `${serverURL}`,
        id: result.skuid,
      };
      params.EventData = JSON.stringify({ skuid });

      skupops.push({
        skuid,
        keys: params.popcode.keys,
      });

      params.Asset = {
        Amount: parseFloat(result.amount),
        Name: result.name,
        UnitOfMeasure: result.uom,
      };
      params.skuid = result.skuid;

      if (result.AuthenticatedServerMode) {
        params.AuthenticatedServerMode = true;
      }

      params.Rules = [{ Name: `*-${networkName}:mintPopcode` }];
      params.Hooks = [{ Name: `*-${networkName}` }];

      Object.assign(k, params);
      const tx = {
        txtype: 'mint',
        txparams: _.cloneDeep(params),
      };

      params.xtra = result.xtra;

      // need to mint here
      await mint(params, ({ txId }) => {
        // add result info to the right spot in story
        tx.txid = txId;
        txlist.push(tx);
        return cb ? cb(story) : null;
      });
    };

    if (args.creator) {
      await execCodes(args);
    } else {
      console.log('Please use the following guide to input the criteria');
      console.log(
        'John (creator) is minting a 100 (amount) kgs (uom) of sugar (name) for his SKU 1234 (skuid)',
      );
      this.prompt(
        [
          {
            type: 'input',
            name: 'creator',
            message: 'Creator ? ',
          },
          {
            type: 'input',
            name: 'amount',
            message: 'amount: How many ? ',
          },
          {
            type: 'input',
            name: 'uom',
            message: 'uom: kgs / lbs ... ? ',
          },
          {
            type: 'input',
            name: 'name',
            message: 'name: seeds / widgets ... ? ',
          },
          {
            type: 'input',
            name: 'skuid',
            message: 'skuid: something unique ... ? ',
          },
        ],
        async result => {
          await execCodes(result, cb);
        },
      );
    }
  });
};
