const bracketUpdate = require('../bctx/bracketUpdate');
const { networkName } = require('../constants');

const params = {
  server: {},
  Rules: [{ Name: `*-${networkName}:updateBracket` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'br.updateBracket',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'event a popcode').action(async function(args, cb) {
    const { server, txlist } = story;
    const execCodes = async result => {
      const k = result;
      params.server = server;
      params.Rules = [{ Name: `*-${networkName}:updateBracket` }];
      params.Hooks = [{ Name: `*-${networkName}` }];
      params.Data = JSON.stringify({ hello: 'world' });
      params.from = result.from;
      params.to = result.to;
      params.type = result.type;
      params.findWith = result.findWith;

      Object.assign(k, params);
      const tx = {
        txtype: 'updateBracket',
        txparams: params,
      };
      await bracketUpdate(params, ({ txId }) => {
        // add result info to the right spot in story
        tx.txid = txId;
        txlist.push(tx);
        return cb ? cb(story) : null;
      });
    };

    if (args.from) {
      await execCodes(args);
    } else {
      this.prompt(
        [
          {
            type: 'input',
            name: 'from',
            message: 'From ? ',
          },
          {
            type: 'input',
            name: 'to',
            message: 'TO ? ',
          },
          {
            type: 'input',
            name: 'type',
            message: 'Type ? ',
          },
          {
            type: 'input',
            name: 'findWith',
            message: 'findWith ? ',
          },
        ],
        async result => {
          await execCodes(result, cb);
        },
      );
    }
  });
};
