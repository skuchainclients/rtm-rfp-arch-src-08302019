const _ = require('lodash');
const { byAddress } = require('../bctx/graphApi');

const params = {
  server: {},
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.queryByAddress',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'queryByAddress').action(async function(args, cb) {
    const { server, txlist } = story;
    const execCodes = async result => {
      params.server = server;
      params.address = result.address;
      const tx = {
        txtype: 'byAddress',
        txparams: _.cloneDeep(params),
      };
      await byAddress(params, ({ popcode }) => {
        // add result info to the right spot in story
        tx.popcode = popcode;
        txlist.push(tx);
        return cb ? cb(story) : null;
      });
    };

    if (args.address) {
      await execCodes(args);
    } else {
      this.prompt(
        [
          {
            type: 'input',
            name: 'address',
            message: 'Address: address ? ',
          },
        ],
        async result => {
          await execCodes(result, cb);
        },
      );
    }
  });
};
