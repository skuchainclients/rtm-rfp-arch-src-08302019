const _ = require('lodash');
const transferBatchOut = require('../bctx/transferBatchOut');
const { networkName, serverURL } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  destPopcode: {
    keys: {},
  },
  sourcePopcode: {
    keys: {},
  },
  sourceBomPopcode: {
    keys: {},
  },
  Data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:transferBatchOut` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

const getPopcode = (tx = [], skuid) => {
  const currentTx = tx.find(t => {
    if (_.has(t, ['txparams', 'skuid'])) {
      const getSkuid = _.get(t, ['txparams', 'skuid']);
      return getSkuid === skuid;
    }
    return null;
  });
  if (currentTx) {
    return _.get(currentTx, ['txparams', 'popcode', 'keys']);
  }
  return null;
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.transferBatchOut',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'event a popcode').action(async function(args, cb) {
    const { server, txlist } = story;
    const execCodes = async result => {
      const k = result;
      params.server = server;

      if (args.sourceeventdata) {
        params.SourceData = JSON.stringify(args.sourceeventdata);
      } else {
        params.SourceData = JSON.stringify({ hello: 'world' });
      }
      if (args.desteventdata) {
        params.DestData = JSON.stringify(args.desteventdata);
      } else {
        params.DestData = JSON.stringify({ hello: 'world' });
      }

      const fromSkuid = {
        domain: `${serverURL}`,
        id: result.from,
      };
      params.SourceEventData = JSON.stringify({ skuid: fromSkuid });
      const toSkuid = {
        domain: `${server}`,
        id: result.to,
      };
      params.DestEventData = JSON.stringify({ skuid: toSkuid });

      params.Amount = parseFloat(result.amount);
      params.Rules = [{ Name: `*-${networkName}:transferBatchOut` }];
      params.Hooks = [{ Name: `*-${networkName}` }];
      const sourcePopcode = getPopcode(txlist, result.from);
      if (!sourcePopcode) {
        throw Error('source popcode not found!');
      }
      const sourceBomPopcode = getPopcode(txlist, result.fromBom);
      if (!sourceBomPopcode) {
        throw Error('source bom popcode not found!');
      }
      const destPopcode = getPopcode(txlist, result.to);
      if (!destPopcode) {
        throw Error('destination popcode not found!');
      }
      params.sourcePopcode.keys = sourcePopcode;
      params.sourceBomPopcode.keys = sourceBomPopcode;
      params.destPopcode.keys = destPopcode;

      Object.assign(k, params);
      const tx = {
        txtype: 'transferBatchOut',
        txparams: _.cloneDeep(params),
      };
      await transferBatchOut(params, ({ txId }) => {
        // add result info to the right spot in story
        tx.txid = txId;
        txlist.push(tx);
        return cb ? cb(story) : null;
      });
    };

    if (args.from) {
      await execCodes(args);
    } else {
      this.prompt(
        [
          {
            type: 'input',
            name: 'from',
            message: 'Source: source skuid ? ',
          },
          {
            type: 'input',
            name: 'to',
            message: 'Destination: destination skuid ? ',
          },
          {
            type: 'input',
            name: 'amount',
            message: 'amount: How many ? ',
          },
        ],
        async result => {
          await execCodes(result, cb);
        },
      );
    }
  });
};
