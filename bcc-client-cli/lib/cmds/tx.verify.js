const _ = require('lodash');
const verify = require('../bctx/verify');
const { networkName } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  popcode: {
    keys: {},
  },
  data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:mintPopcode` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'tx.verify',
  };

  // eslint-disable-next-line func-names
  vorpal
    .command(cmd.name, 'verifies a transaction')
    .action(async (args, cb) => {
      const { server, txlist } = story;

      params.txlist = txlist;

      const execCodes = async result => {
        console.log(`Verifying a transaction: ${result.name}`);
        const k = result;

        params.server = server;

        Object.assign(k, params);
        const tx = {
          txtype: 'verify',
          txparams: _.cloneDeep(params),
        };
        // need to mint here
        await verify(params, res => {
          // add result info to the right spot in story
          tx.res = res;
          txlist.push(tx);
          return cb ? cb(story) : null;
        });
      };
      await execCodes(args);
    });
};
