const fs = require('fs');
const _ = require('lodash');

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'story.load',
  };

  vorpal.command(cmd.name, 'loads a story').action((args, cb) => {
    this.log(cmd.name);
    const self = this;
    this.prompt(
      {
        type: 'input',
        name: 'file',
        message: 'filename with story to load? ',
      },
      result => {
        self.log(`Okay, ${JSON.stringify(result)} it is!`);
        const json = fs.readFileSync(result.file);
        const storyJson = JSON.parse(json);
        if (storyJson.server) {
          story.server = storyJson.server;
        }
        if (storyJson.users) {
          story.users = storyJson.users;
        }
        if (storyJson.popcodes) {
          story.popcodes = _.cloneDeep(storyJson.popcodes);
        }
        if (storyJson.callstack) {
          story.callstack = storyJson.callstack;
        }
        if (storyJson.txlist) {
          story.txlist = _.cloneDeep(storyJson.txlist);
        }
        console.log('print after', story);
        cb();
      },
    );
  });
};
