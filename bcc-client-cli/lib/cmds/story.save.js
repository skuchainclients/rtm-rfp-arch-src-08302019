const fs = require('fs');

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'story.save',
  };

  vorpal.command(cmd.name, 'saves a story').action((args, cb) => {
    console.log('Saving story..');
    const execCodes = result => {
      const { file } = result;
      fs.writeFileSync(file, JSON.stringify(story, null, 2));
      return cb ? cb(story) : null;
    };
    if (args.file) {
      execCodes(args);
    } else {
      this.prompt(
        {
          type: 'input',
          name: 'file',
          message: 'filename to save story to? ',
        },
        result => {
          execCodes(result);
        },
      );
    }
  });
};
