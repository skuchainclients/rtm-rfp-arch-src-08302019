module.exports = (vorpal, story) => {
  const cmd = {
    name: `pre.exec`,
  };

  const execUser = name =>
    new Promise(res => {
      res(vorpal.execSync('user.create', { name }));
    });

  const execPrint = newStory =>
    new Promise(res => {
      res(vorpal.execSync('story.print', { story: newStory }));
    });
  const execSave = (file = './storyData/pre.story.json') =>
    new Promise(res => {
      res(vorpal.execSync('story.save', { file }));
    });

  vorpal.command(cmd.name, 'exec story').action(async (args, cb) => {
    vorpal.execSync('user.create', { name: 'foo' });
    vorpal.execSync('user.create', { name: 't1' });
    vorpal.execSync('story.save', { file: './storyData/pre1.json' });
    cb();
  });
};
