const _ = require('lodash');
const transferBom = require('../bctx/transferBom');
const { networkName } = require('../constants');

const params = {
  server: {},
  creator: {
    keys: {},
  },
  destPopcode: {
    keys: {},
  },
  sourcePopcode: {
    keys: {},
  },
  Data: JSON.stringify({ hello: 'world' }),
  Rules: [{ Name: `*-${networkName}:transferBom` }],
  Hooks: [{ Name: `*-${networkName}` }],
};

const getPopcode = (tx = [], skuid) => {
  const currentTx = tx.find(t => {
    if (_.has(t, ['txparams', 'skuid'])) {
      const getSkuid = _.get(t, ['txparams', 'skuid']);
      return getSkuid === skuid;
    }
    return null;
  });
  if (currentTx) {
    return _.get(currentTx, ['txparams', 'popcode', 'keys']);
  }
  return null;
};

module.exports = (vorpal, story) => {
  const cmd = {
    name: 'pc.transferBom',
  };

  // eslint-disable-next-line func-names
  vorpal
    .command(cmd.name, 'transfer value popcode')
    .action(async (args, cb) => {
      const { server, txlist } = story;
      const execCodes = async result => {
        const k = result;
        params.server = server;

        if (args.data) {
          params.Data = JSON.stringify(args.data);
        } else {
          params.Data = JSON.stringify({ hello: 'world' });
        }

        if (args.eventdata) {
          params.EventData = JSON.stringify(args.eventdata);
        } else {
          params.EventData = JSON.stringify({});
        }

        params.SourceAmount = parseFloat(result.sourceAmount);
        params.DestAmount = parseFloat(result.destAmount);
        params.Rules = [{ Name: `*-${networkName}:transferBom` }];
        params.Hooks = [{ Name: `*-${networkName}` }];
        const sourcePopcode = getPopcode(txlist, result.from);
        if (!sourcePopcode) {
          throw Error('source popcode not found!');
        }
        const destPopcode = getPopcode(txlist, result.to);
        if (!destPopcode) {
          throw Error('destination popcode not found!');
        }
        params.sourcePopcode.keys = sourcePopcode;
        params.destPopcode.keys = destPopcode;

        Object.assign(k, params);
        const tx = {
          txtype: 'transferBom',
          txparams: _.cloneDeep(params),
        };
        await transferBom(params, ({ txId }) => {
          // add result info to the right spot in story
          tx.txid = txId;
          txlist.push(tx);
          return cb ? cb(story) : null;
        });
      };

      if (args.from) {
        await execCodes(args);
      } else {
        this.prompt(
          [
            {
              type: 'input',
              name: 'from',
              message: 'Source: source skuid ? ',
            },
            {
              type: 'input',
              name: 'to',
              message: 'Destination: destination skuid ? ',
            },
            {
              type: 'input',
              name: 'destAmount',
              message: 'amount: How many ? ',
            },
            {
              type: 'input',
              name: 'sourceAmount',
              message: 'amount: How many ? ',
            },
          ],
          async result => {
            await execCodes(result, cb);
          },
        );
      }
    });
};
