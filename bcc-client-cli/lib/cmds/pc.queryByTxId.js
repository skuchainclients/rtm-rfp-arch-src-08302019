const _ = require('lodash');
const { byTxId } = require('../bctx/graphApi');

const params = {
  server: {},
};

module.exports = (vorpal, { txlist, server }) => {
  const cmd = {
    name: 'pc.byTxId',
  };

  // eslint-disable-next-line func-names
  vorpal.command(cmd.name, 'queryByTxId').action(function(args, cb) {
    this.prompt(
      [
        {
          type: 'input',
          name: 'txId',
          message: 'tx ? ',
        },
      ],
      result => {
        params.server = server;
        params.txId = result.txId;
        const tx = {
          txtype: 'queryByTxId',
          txparams: _.cloneDeep(params),
        };
        byTxId(params, data => {
          console.log(data);
          tx.data = data;
          txlist.push(tx);
          cb();
        });
      },
    );
  });
};
