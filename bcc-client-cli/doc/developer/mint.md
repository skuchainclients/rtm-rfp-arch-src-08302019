## Creating the payload

The mint operation accepts the following payload

```
{
	"Popcode": {
		"Asset": {
			"Amount": 0,
			"Name": "string",
			"UnitOfMeasure": "string"
		},
		"BOM": [{
			"PopcodeAddress": "string",
			"SKU": {
				"Amount": 0,
				"Name": "string",
				"UnitOfMeasure": "string"
			}
		}],
		"CreatorPubKey": "string",
		"CreatorSig": "string",
		"Counter": "string",
		"Data": "string",
		"EventData": "string",
		"Owners": [{
			"OwnerPubKey": "string",
			"Quantity": 0
		}],
		"PopcodeAddress": "string",
		"PrevCounter": "string",
		"Quality": 0,
		"Threshold": 0
	},
	"Rules": [{
		"Name": "string",
		"Params": "string"
	}],
	"Hooks": [{
		"Name": "string"
	}],
	"AuthenticatedServerMode": true
}
```

### Popcode

#### Asset
In our [example](../scenarios/mint.md) Alice produces 10L of milk, this translates to

```
{
	"Popcode": {
		"Asset": {
			"Amount": 10,
			"Name": "milk",
			"UnitOfMeasure": "liters"
		},
		"BOM": [{
		...
```

#### Bom
We can ignore this field for now
```
...
	"UnitOfMeasure": "string"
		},
		"BOM": [],
		"CreatorPubKey": "string",
		....
```

#### CreatorPublicKey,

This is the public key used for the digital signature verification. To initially get keys, we can utilize the `createKeys()` function from `bcc-client-lib`

```
const keys = bc3lib.keys.createKeys();
const creatorPublicKey = keys.publicKey;
```
This would translate to

```
{
	"Popcode": {
		"Asset": {
			"Amount": 10,
			"Name": "milk",
			"UnitOfMeasure": "liters"
		},
		"BOM": [],
		"CreatorPubKey": "03f216adab17a32382ee8e73c239caf3904945dc0a2998d20997830fac2224a01b",
		...
```

#### CreatorSig
To write a transaction to the blockchain, it must be digitally signed. To create a signature we use the `bc3lib.tx.MintTX.sign()` function from `bcc-client-lib`

```
// Get a mintTX signature

const  sig  =  bc3lib.tx.MintTX.sign({
	Counter:  counter,
	popcode:  pc,
	privateKey,
});
```

Now at this point we do not have a `Counter` or `popcode`

#### Counter
In order to get the counter, we need to make a call to `/balance`. To make the call we create a GET request /balance?popcodeAddress=_address_

```
const balance = await  axios.get(`${server}/balance`, { params })
const counter = balance.data.Counter;
...
```

#### Data
The data field can be any string that is stored along with the popcode on the blockchain. Following our example we use

```
...
"data": {
			"creator": "Alice",
			"amount": "10",
			"uom": "liters",
			"name": "milk",
			"skuid": "id1",
		...
```

#### eventData

Event Data is not stored in the blockchain but is stored in persistence after the transaction is recorded on the blockchain. Following our example we have

```
"eventdata": {
				"DATE": "2019-03-30",
				"SourceUnit": "C123",
				"QUANTITY": "10",
				"NF/CENTER": "AliceWarehouse1"
			}
```

#### Owners

We can ignore the the owners field for our example

#### Popcode Address

This was generated in our call to `createKeys()`. From above

```
const keys = bc3lib.keys.createKeys();
const creatorPublicKey = keys.publicKey;
const address = keys.address;
```

At this point we have enough to create a signature

```

const  pc  = {
	PopcodeAddress,
	Asset,
	Data,
	CreatorPubKey,
};

const  sig  =  bc3lib.tx.MintTX.sign({
	Counter:  counter,
	popcode:  pc,
	privateKey,
});
```

At this point our payload looks like

```
{
	"Popcode": {
		"Asset": {
			"Amount": 10,
			"Name": "milk",
			"UnitOfMeasure": "liters"
		},
		"BOM": [],
		"CreatorPubKey": "03f216adab17a32382ee8e73c239caf3904945dc0a2998d20997830fac2224a01b",
		"CreatorSig": "3045022100efccae3317de716e224da1a6b97fef91611dafb44277d7a3374f93a7de31902802201168b0f8aad9715c27f7955bd7114d9050c48d2b3750c0402e19e3f5dbe64183",
		"Counter": "7e9e6e05eba482df60caa557a75f9aa92dfa3440c252ef49bed5dcc30dd859ef",
		"Data": "{\"data\":{\"creator\":\"Alice\",\"amount\":\"10\",\"uom\":\"liters\",\"name\":\"milk\",\"skuid\":\"id1\"}}",
		"EventData": "{\"eventdata\":{\"DATE\":\"2019-03-30\",\"SourceUnit\":\"C123\",\"QUANTITY\":\"10\",\"NF/CENTER\":\"AliceWarehouse1\"}}",
		"Owners": [],
		"PopcodeAddress": "5031bf9c454de8fb860dee5310e4f7f4913bea4d",
		...
```

#### PrevCounter, Threshold, Quality

We can ignore these fields for this example

#### Rules and Hooks
Rules are used for post ledger write operations. For example sending notifications would be set in the rules field. In our example we are not creating rules. Hooks specifies which post ledger system would execute those rules. We can leave this field blank.

### AuthenticatedServerMode

This flag is set for batch transactions. We can leave this field out altogether for our example.

### Putting it all together

```
{
	"Popcode": {
		"Asset": {
			"Amount": 10,
			"Name": "milk",
			"UnitOfMeasure": "liters"
		},
		"BOM": [],
		"CreatorPubKey": "03f216adab17a32382ee8e73c239caf3904945dc0a2998d20997830fac2224a01b",
		"CreatorSig": "3045022100efccae3317de716e224da1a6b97fef91611dafb44277d7a3374f93a7de31902802201168b0f8aad9715c27f7955bd7114d9050c48d2b3750c0402e19e3f5dbe64183",
		"Counter": "7e9e6e05eba482df60caa557a75f9aa92dfa3440c252ef49bed5dcc30dd859ef",
		"Data": "{\"data\":{\"creator\":\"Alice\",\"amount\":\"10\",\"uom\":\"liters\",\"name\":\"milk\",\"skuid\":\"id1\"}}",
		"EventData": "{\"eventdata\":{\"DATE\":\"2019-03-30\",\"SourceUnit\":\"C123\",\"QUANTITY\":\"10\",\"NF/CENTER\":\"AliceWarehouse1\"}}",
		"Owners": [],
		"PopcodeAddress": "5031bf9c454de8fb860dee5310e4f7f4913bea4d",
		"PrevCounter": "string",
		"Quality": 0,
		"Threshold": 0
	},
	"Rules": [],
	"Hooks": []
}
```

### Running the example

The example app splits the values creation from the operation to call

#### Create the values

Use [pc.mint.js](../../lib/cmds/pc.mint.js) to create the values in the correct format 

#### Call mint
  
Now call [mint.js](../../lib/bctx/mint.js) passing in those parameters from the previous step.

### Execute 

``` 
$> node vrun.js --runfile=./stories/alice-01.json
Staring with a blank story
Loading run from... ./stories/alice-01.json
doing.. user.create
done.. user.create
doing.. pc.mint
Minting a popcode: milk
counter  7e9e6e05eba482df60caa557a75f9aa92dfa3440c252ef49bed5dcc30dd859ef
done.. pc.mint
doing.. story.save
Saving story..
done.. story.save
```

Your story will appear in the `/output` folder