## Pre-requisite

In order to initiate a transfer unit request, there must be a source popcode and a destination popcode


- [get two popcodes](#Mint)
- [create transfer values](#Values)
- [post to API](#Post-To-API)

## Mint

To create mint two popcodes, see [mint](./mint.md)


## Values

Create the values to transfer, for example Alice is tranferring 10L of milk to Bob. Transfer Unit only handles the same data type so we only need to pass through the amount.

### code

```
const params = {
    Amount: 10,
    DestPopcodeAddress: destPopcode.address,
    DestData: JSON.stringify({ source: 'world' }),
    DestEventData: JSON.stringify({ metadata: { spot: 'dest' } }),
    SourcePopcodeAddress: sourcePopcode.address,
    SourceData: JSON.stringify({ source: 'world' }),
    SourceEventData: JSON.stringify({ metadata: { spot: 'source' } }),
    Rules: [
      {
        Name: '*-<SERVER>:transformo',
      },
    ],
    Hooks: [{ Name: '*-<SERVER>' }],
  };
```

### code

```
const  mintPL  = {
	Popcode: {
		Asset,
		CreatorPubKey,
		CreatorSig,
		Counter,
		Data,
		EventData,
		PopcodeAddress,
	},
	Rules,
	Hooks,
};
```

## Sign Payload

In order to write the operation to the blockchain we need to digitally sign the payload. To sign the payload we use `bc3lib.tx.MintTX.sign()`

### code
```
// Payload
const  pc  = {
	PopcodeAddress,
	Asset,
	Data,
	CreatorPubKey,
};

// Get a mintTX signature
const  sig  =  bc3lib.tx.MintTX.sign({
	Counter:  counter,
	popcode:  pc,
	privateKey,
});
pc.CreatorSig  =  sig;
```

## Post To API

With both the signature and the payload we can post to the `bcc-tuxedopopsware` endpoint via axios.

### code

```
axios.post(`${tpware}/mint`, mintPL);
```

## Putting it altogether

- With your own keys include [mint.js](../../lib/bctx/transfer_unit.js) in your application to initiate a transfer_unit operation.

## User story

For a business case story and example values see the product [mint](../product/mint.md) description