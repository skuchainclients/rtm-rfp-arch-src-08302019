### Verifying a transaction

1. Execute any of the scenarios
2. Pass any of the outputs as a storybase
2. Verify the popcode can be found in dgraph

#### Search dGraph for the result of the operation

- Command: `tx.verify`
- storyfile: [verify.json](../../stories/verify.json)

```
$> node vrun.js --storybase=./stories/output/batchOut-02.json --storyfile=./stories/verify.json
```

Note we have specified the output to live in [verify](../../stories/output/verify-01.json)