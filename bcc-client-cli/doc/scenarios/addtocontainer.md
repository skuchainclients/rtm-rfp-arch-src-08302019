## Creating a scenario

### 10L milk from Alice is deposited in a warehouse

1. Create a key pair for Alice in order to sign the digital transaction
1. Create a key pair for the owner of the transportation vehicle
2. Create a key pair for owner of the warehouse
3. Alice creates (_mints_) 10L of milk
4. Truck owner creates/supplies truck
5. Warehouse owner creates/supplies warehouse address
4. Alice transfers (_transferUnit_) 10L of milk to the truck
5. The truck transfers 10L to the warehouse

#### No users have been created

1. Create user for Alice
```
$> node vrun.js --storyfile=./stories/alice.json
```
2. Trucking company
```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/truck.json
```
3. Warehouse proprietor
```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/warehouse.json
```

Notice when creating the second user, we can pass the output of the first storyfile as an input to `vrun.js`. This enables us to build our story

#### Users have been created

If users already exist, we can use the user [storybase](../../stories/output/users.json) previously created 

#### Alice creates 10L of milk

- Command: `pc.mint`
- storyfile: [amr-01.json](../../stories/amr-01.json)

1. Have previous [storybase](../../stories/output/users.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-01.json)

```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/amr-01.json
```

Note we have specified the output to live in [mint-01](../../stories/output/mint-01.json)

#### Trucking company supplies/creates a truck

- Command: `pc.mint`
- storybase: [amr-05.json](../../stories/amr-05.json)

1. Have previous [storybase](../../stories/output/mint-01.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-05.json)

```
$> node vrun.js --storybase=./stories/output/mint-01.json --storyfile=./stories/amr-05.json
```

Note we have specified the output to live in [batchIn-01](../../stories/output/batchIn-01.json)

#### Warehouse propietor supplies/creates a warehouse

- Command: `pc.mint`
- storybase: [amr-06.json](../../stories/amr-06.json)

1. Have previous [storybase](../../stories/output/batchIn-01.json) containing previous mint operations
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-06.json)

```
$> node vrun.js --storybase=./stories/output/batchIn-01.json --storyfile=./stories/amr-06.json
```

Note we have specified the output to live in [batchIn-01](../../stories/output/batchIn-01.json)



#### Alice transfers 10L of milk to Truck

- Command: `pc.transferUnit`
- storybase: [amr-07.json](../../stories/amr-07.json)

1. Have previous [storybase](../../stories/output/mint-01.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-05.json)

```
$> node vrun.js --storybase=./stories/output/batchIn-02.json --storyfile=./stories/amr-07.json
```

Note we have specified the output to live in [batchIn-03](../../stories/output/batchIn-03.json)

#### Truck deposits milk at Warehouse

- Command: `pc.transferBatchIn`
- storybase: [amr-08.json](../../stories/amr-08.json)

1. Have previous [storybase](../../stories/output/batchIn-03.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-08.json)

```
$> node vrun.js --storybase=./stories/output/batchIn-03.json --storyfile=./stories/amr-08.json
```

Note we have specified the output to live in [batchIn-04](../../stories/output/batchIn-04.json)