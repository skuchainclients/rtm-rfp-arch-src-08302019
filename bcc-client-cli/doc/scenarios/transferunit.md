## Creating a scenario

### Alice transfers 10L of milk to Bob

1. Create a key pair for Alice in order to sign the digital transaction
2. Create a key pair for Bob in order to sign the digital transaction
3. Alice creates (_mints_) 10L of milk
4. Bob creates (_mints_)  or uses a collector of the milk
5. Alice then transfers (_transferUnit_) 10L of milk to Bob

#### No users have been created

1. Create user for Alice
```
$> node vrun.js --storyfile=./stories/alice.json
```
2. Create user for Bob
```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/bob.json
```

Notice when creating the second user, we can pass the output of the first storyfile as an input to `vrun.js`. This enables us to build our story

#### Users have been created

If users already exist, we can use the user [storybase](../../stories/output/users.json) previously created 

#### Alice creates 10L of milk

- Command: `pc.mint`
- storyfile: [amr-01.json](../../stories/amr-01.json)

1. Have previous [storybase](../../stories/output/users.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-01.json)

```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/amr-01.json
```

Note we have specified the output to live in [mint-01](../../stories/output/mint-01.json)

#### Bob specifies receiving container

- Command: `pc.mint`
- storyfile: [amr-02.json](../../stories/amr-02.json)

```
$> node vrun.js --storybase=./stories/output/mint-01.json --storyfile=./stories/amr-02.json
```

Note we have specified the output to live in [mint-02](../../stories/output/mint-02.json)

#### Alice transfers 10L of milk to Bob

- Command: `pc.transferUnit`
- storyfile: [amr-03.json](../../stories/amr-03.json)

```
$> node vrun.js --storybase=./stories/output/mint-02.json --storyfile=./stories/amr-03.json
```

Note: The `from` and `to` values must match the `skuid` values from the mint. Ex:

[amr-01.json](../../stories/amr-01.json) has a `skuid` of `id1` and [amr-02.json](../../stories/amr-02.json) has a `skuid` of `id2`

the [amr-03.json](../../stories/amr-03.json) must use those values in the from/to

```
...
"data": {
	"from": "id1",
	"to": "id2",
	"amount": "10"
}
      ...
```

We now inspect output to see a list of our transactions