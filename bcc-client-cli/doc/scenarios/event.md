## Creating a scenario

### Alice adds event information to an operation

1. Create a key pair for Alice in order to sign the digital transaction
2. Alice creates (_mints_) 10L of milk
3. Alice notes she plans to send the milk to Bob

#### No users have been created

1. Create user for Alice
```
$> node vrun.js --storyfile=./stories/alice.json
```

Notice when creating the second user, we can pass the output of the first storyfile as an input to `vrun.js`. This enables us to build our story

#### Users have been created

If users already exist, we can use the user [storyfile](../../stories/output/users.json) previously created 

#### Alice creates 10L of milk

- Command: `pc.mint`
- storyfile: [amr-01.json](../../stories/amr-01.json)

1. Have previous [storyfile](../../stories/output/users.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-01.json)

```
$> node vrun.js --storybase=./stories/output/users.json --storyfile=./stories/amr-01.json
```

Note we have specified the output to live in [mint-01](../../stories/output/mint-01.json)

#### Alice creates event message(s)

- Command: `pc.event`
- storyfile: [amr-04.json](../../stories/amr-04.json)

```
$> node vrun.js --storybase=./stories/output/mint-01.json --storyfile=./stories/amr-04.json
```

Note: The `skuid` values must match the `skuid` values from the mint. Ex:

[amr-01.json](../../stories/amr-01.json) has a `skuid` of `id1` 

the [amr-04.json](../../stories/amr-04.json) must use that values in the `skuid`

```
...
"data": {
	"skuid": "id1",
    ...
}
      ...
```

We now inspect output to see a list of our transactions


### Troubleshooting

```
(node:34661) UnhandledPromiseRejectionWarning: Unhandled promise rejection (rejection id: 2): Error: Popcode not found!
(node:34661) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
```

The `skuid` values from a previously minted popcode and the `pc.event` do not match.