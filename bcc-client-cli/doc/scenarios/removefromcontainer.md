### 10L milk from Warehouse is put on a ship

A warehouse can store many items (_popcodes_)

#### Pre-requisite

Follow the steps from  [add to container](./addtocontainer.md) to place milk in the warehouse


1. Create a key pair for the owner of the ship
2. Ship owner creates/supplies ship
3. Warehouse owner transfers milk to the ship


1. Create user for Ship
```
$> node vrun.js --storybase=./stories/output/batchIn-04.json --storyfile=./stories/ship.json
```

Notice when creating the second user, we can pass the output of the first storyfile as an input to `vrun.js`. This enables us to build our story

#### Shipping company supplies/creates a ship

- Command: `pc.mint`
- storybase: [amr-09.json](../../stories/amr-09.json)

1. Have previous [storybase](../../stories/output/batchIn-04.json) containing user information
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-09.json)

```
$> node vrun.js --storybase=./stories/output/batchOut-01.json --storyfile=./stories/amr-09.json
```

Note we have specified the output to live in [batchOut-02](../../stories/output/batchOut-02.json)

#### Warehouse transfers 

- Command: `pc.transferBatchOut`
- storybase: [amr-10.json](../../stories/amr-10.json)

1. Have previous [storybase](../../stories/output/batchOut-02.json) containing user operations up to the point of milk in the warehouse
2. Create values and metadata to describe operation in a new [storyfile](../../stories/amr-10.json)

```
$> node vrun.js --storybase=./stories/output/batchOut-02.json --storyfile=./stories/amr-10.json
```

Note we have specified the output to live in [batchOut-03](../../stories/output/batchOut-03.json)