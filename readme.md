# Architecture and chain code interface

Please find enclosed the following

- Skuchain-ec3-architecture.pdf
  
  This provides an overview of the dat flow and microservice architecture of the EC3 platform

- kvware.yml, tpware.yml

  This provides the api spec in swagger yaml format. This api is the interface to be used by application developers when the send transactions to the blockchaon

-  bcc-client-cli

   This provides sample interface code that generates payloads and uses the api to submit transactions to the blockchain

- kevlar

    This is the chain code source code for writing key value assertions to the blockchain


# Confidentiality statement

All files and information provided herein is of a confidential and proprietary nature and provided solely for the purposes of responding to the RFP. 
