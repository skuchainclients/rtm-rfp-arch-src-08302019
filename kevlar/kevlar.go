/*
Copyright (c) 2016 Skuchain,Inc

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"

	"encoding/json"

	ElementProof "bitbucket.org/skuchaindev/kevlar/ProofElements"
	pbg "bitbucket.org/skuchaindev/kevlar/protos"
	"github.com/btcsuite/btcd/btcec"
	"github.com/golang/protobuf/proto"
)

// This chaincode implements the ledger operations for the proofchaincode

type kevlarChainCodeEvent struct {
	Function     string
	Proof        pbg.ProofTX
	SecpProof    *ElementProof.SecP256k1ElementProof
	SecpShaProof *ElementProof.SecP256k1SHA2ElementProof
}

// ProofChainCode example simple Chaincode implementation
type kevlarChainCode struct {
	logger *shim.ChaincodeLogger
}

func prettyprint(b []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "  ")
	return out.Bytes(), err
}

func (t *kevlarChainCode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

//ProofChainCode.Invoke runs a transaction against the current state
func (t *kevlarChainCode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {

	function, args := stub.GetFunctionAndParameters()

	if function == "query" {
		return t.query(stub, args)
	}

	//Proofs Chaincode should have one transaction argument. This is body of serialized protobuf
	if len(args) == 0 {
		t.logger.Error("Zero arguments found")
		return shim.Error("Zero arguments found")
	}

	argsBytes, err := hex.DecodeString(args[0])
	if err != nil {
		t.logger.Error("Invalid argument expected hex")
		return shim.Error("Invalid argument expected hex")
	}
	argsProof := pbg.ProofTX{}
	err = proto.Unmarshal(argsBytes, &argsProof)
	if err != nil {
		t.logger.Error("Invalid argument expected protocol buffer")
		return shim.Error("Invalid argument expected protocol buffer")
	}
	t.logger.Debug("********************** debug chaincode")
	t.logger.Debug(function)
	t.logger.Debug(argsProof)

	chaincodeEvent := kevlarChainCodeEvent{Function: function, Proof: argsProof, SecpProof: nil, SecpShaProof: nil}

	//fmt.Printf("ok", chaincodeEvent)

	switch function {

	case "createProof":
		name := argsProof.Name
		nameCheckBytes, err := stub.GetState("Proof:" + name)
		if len(nameCheckBytes) != 0 {
			fmt.Printf("Proof Name:%s already claimed\n", name)
			return shim.Error(fmt.Sprintf("Proof Name:%s already claimed\n", name))
		}
		threshold := argsProof.Threshold
		publicKeys := argsProof.PubKeys
		if int(threshold) > len(publicKeys) {
			fmt.Printf("Invalid Threshold of %d for %d keys\n", threshold, len(publicKeys))
			return shim.Error(fmt.Sprintf("Invalid Threshold of %d for %d keys\n", threshold, len(publicKeys)))
		}
		switch argsProof.Type {
		case pbg.ProofType_SECP256K1:
			newProof := new(ElementProof.SecP256k1ElementProof)
			newProof.ProofName = name
			newProof.State = ElementProof.Initialized
			newProof.Threshold = int(threshold)
			for _, keybytes := range publicKeys {
				pubKey, errF := btcec.ParsePubKey(keybytes, btcec.S256())
				if errF != nil {
					fmt.Printf("Invalid Public Key: %v\n", keybytes)
					return shim.Error(fmt.Sprintf("Invalid Public Key: %v", keybytes))
				}
				newProof.PublicKeys = append(newProof.PublicKeys, *pubKey)
			}
			bufferData := newProof.ToBytes()
			err = stub.PutState("Proof:"+name, bufferData)
			if err != nil {
				fmt.Printf("Error Saving Proof to Data %s\n", err)
				return shim.Error(fmt.Sprintf("Error Saving Proof to Data %s", err))
			}
			chaincodeEvent.SecpProof = newProof
		case pbg.ProofType_SECP256K1SHA2:
			t.logger.Debug("Creating Sha2 Proof")
			newProof := ElementProof.SecP256k1SHA2ElementProof{}
			newProof.ProofName = name
			newProof.State = ElementProof.Initialized
			newProof.Threshold = int(threshold)

			for _, keybytes := range publicKeys {
				pubKey, errF := btcec.ParsePubKey(keybytes, btcec.S256())
				if errF != nil {
					fmt.Printf("Invalid Public Key: %v\n", keybytes)
					return shim.Error(fmt.Sprintf("Invalid Public Key: %v", keybytes))
				}
				newProof.PublicKeys = append(newProof.PublicKeys, *pubKey)
			}

			for _, digest := range argsProof.Digests {
				if len(digest) != 32 {
					t.logger.Error("Invalid Digest Length")
					return shim.Error(fmt.Sprintf("Invalid Digest Length"))
				}
				var fixedDigest [32]byte
				copy(fixedDigest[:], digest)
				newProof.Digests = append(newProof.Digests, fixedDigest)
			}

			bufferData := newProof.ToBytes()
			err = stub.PutState("Proof:"+name, bufferData)
			if err != nil {
				fmt.Printf("Error Saving Proof to Data %s\n", err)
				return shim.Error(fmt.Sprintf("Error Saving Proof to Data %s", err))
			}
			chaincodeEvent.SecpShaProof = &newProof
		default:
			t.logger.Error("Invalid Proof Type")
			return shim.Error("Invalid Proof Type")
		}

	case "signProof":
		proofBytes, err := stub.GetState("Proof:" + argsProof.Name)
		if err != nil || len(proofBytes) == 0 {
			fmt.Printf("Could not retrieve:%s\n", argsProof.Name)
			return shim.Error(fmt.Sprintf("Could not retrieve:%s", argsProof.Name))
		}

		secpProof := new(ElementProof.SecP256k1ElementProof)
		secpShaProof := new(ElementProof.SecP256k1SHA2ElementProof)

		err = secpProof.FromBytes(proofBytes)
		if err == nil {
			result := secpProof.Signed(&argsProof.Signatures, argsProof.Data)
			if result == false {
				t.logger.Error("Invalid Signatures")
				return shim.Error("Invalid Signatures")
			}
			proofBytes = secpProof.ToBytes()

			stub.PutState("Proof:"+secpProof.Name(), proofBytes)
		}

		err = secpShaProof.FromBytes(proofBytes)
		if err == nil {
			result := secpShaProof.Signed(&argsProof.Signatures, argsProof.Data)
			if result == false {
				t.logger.Error("Invalid Signatures")
				return shim.Error("Invalid Signatures")
			}
			result = secpShaProof.Hash(argsProof.PreImages)
			if result == false {
				t.logger.Error("Invalid Preimages")
				return shim.Error("Invalid Preimages")
			}
			proofBytes = secpShaProof.ToBytes()
			stub.PutState("Proof:"+secpShaProof.Name(), proofBytes)
		}

		chaincodeEvent.SecpProof = secpProof
		chaincodeEvent.SecpShaProof = secpShaProof

		//return shim.Success(nil)

	case "revokeProof":
		proofBytes, err := stub.GetState("Proof:" + argsProof.Name)
		if err != nil || len(proofBytes) == 0 {
			fmt.Printf("Could not retrieve:%s\n", argsProof.Name)
			return shim.Error(fmt.Sprintf("Could not retrieve:%s", argsProof.Name))
		}

		secpProof := new(ElementProof.SecP256k1ElementProof)
		secpShaProof := new(ElementProof.SecP256k1SHA2ElementProof)

		err = secpProof.FromBytes(proofBytes)
		if err == nil {
			result := secpProof.Revoked(&argsProof.Signatures)
			if result == false {
				return shim.Error("Invalid Signatures")
			}
			proofBytes = secpProof.ToBytes()

			stub.PutState("Proof:"+secpProof.Name(), proofBytes)
		}

		err = secpShaProof.FromBytes(proofBytes)
		if err == nil {
			result := secpShaProof.Revoked(&argsProof.Signatures)
			if result == false {
				t.logger.Error("Invalid Signatures")
				return shim.Error("Invalid Signatures")
			}
			proofBytes = secpShaProof.ToBytes()
			stub.PutState("Proof:"+secpShaProof.Name(), proofBytes)
		}

		chaincodeEvent.SecpProof = secpProof
		chaincodeEvent.SecpShaProof = secpShaProof

		//return shim.Success(nil)

	case "supercedeProof":

		proofBytes, err := stub.GetState("Proof:" + argsProof.Name)
		if err != nil || len(proofBytes) == 0 {
			fmt.Printf("Could not retrieve:%s\n", argsProof.Name)
			return shim.Error(fmt.Sprintf("Could not retrieve:%s", argsProof.Name))
		}

		nameCheck, err := stub.GetState("Proof:" + argsProof.Supercede.Name)
		if err != nil || len(nameCheck) == 0 {
			fmt.Printf("Could not retrieve:%s\n", argsProof.Supercede.Name)
			return shim.Error(fmt.Sprintf("Could not retrieve:%s", argsProof.Name))
		}
		secpProof := new(ElementProof.SecP256k1ElementProof)
		secpShaProof := new(ElementProof.SecP256k1SHA2ElementProof)

		supercededBits, err := proto.Marshal(argsProof.GetSupercede())
		supercedeDigest := sha256.Sum256(supercededBits)
		digestHex := hex.EncodeToString(supercedeDigest[:])

		name := argsProof.Supercede.Name
		threshold := argsProof.Supercede.Threshold
		publicKeys := argsProof.Supercede.PubKeys

		if int(threshold) > len(publicKeys) {
			fmt.Printf("Invalid Threshold of %d for %d keys\n", threshold, len(publicKeys))
			return shim.Error(fmt.Sprintf("Invalid Threshold of %d for %d keys ", threshold, len(publicKeys)))
		}

		var bufferData []byte
		switch argsProof.Supercede.Type {
		case pbg.ProofType_SECP256K1:
			newProof := new(ElementProof.SecP256k1ElementProof)
			newProof.ProofName = name
			newProof.State = ElementProof.Initialized
			newProof.Threshold = int(threshold)
			for _, keybytes := range publicKeys {
				pubKey, errF := btcec.ParsePubKey(keybytes, btcec.S256())
				if errF != nil {
					fmt.Printf("Invalid Public Key: %v\n", keybytes)
					return shim.Error(fmt.Sprintf("Invalid Public Key: %v", keybytes))
				}
				newProof.PublicKeys = append(newProof.PublicKeys, *pubKey)
			}
			bufferData = newProof.ToBytes()

		case pbg.ProofType_SECP256K1SHA2:
			newProof := ElementProof.SecP256k1SHA2ElementProof{}
			newProof.ProofName = name
			newProof.State = ElementProof.Initialized
			newProof.Threshold = int(threshold)

			for _, keybytes := range publicKeys {
				pubKey, errF := btcec.ParsePubKey(keybytes, btcec.S256())
				if errF != nil {
					fmt.Printf("Invalid Public Key: %v\n", keybytes)
					return shim.Error(fmt.Sprintf("Invalid Public Key: %v", keybytes))
				}
				newProof.PublicKeys = append(newProof.PublicKeys, *pubKey)
			}

			for _, digest := range argsProof.Supercede.Digests {
				if len(digest) != 32 {
					t.logger.Error("Invalid Digest Length")
					return shim.Error("Invalid Digest Length")
				}
				var fixedDigest [32]byte
				copy(fixedDigest[:], digest)
				newProof.Digests = append(newProof.Digests, fixedDigest)
			}

			bufferData = newProof.ToBytes()

		default:
			t.logger.Error("Invalid Proof Type")
			return shim.Error("Invalid Proof Type")
		}

		err = secpProof.FromBytes(proofBytes)
		if err == nil {
			result := secpProof.Supercede(&argsProof.Signatures, name)
			if result == false {
				fmt.Printf("Invalid Signatures. Digest: %s\n", digestHex)
				return shim.Error("Invalid Signatures")
			}
			proofBytes = secpProof.ToBytes()

			stub.PutState("Proof:"+secpProof.Name(), proofBytes)
		}

		err = secpShaProof.FromBytes(proofBytes)
		if err == nil {
			result := secpShaProof.Supercede(&argsProof.Signatures, name)
			if result == false {
				fmt.Printf("Invalid Signatures. Digest: %s\n", digestHex)
				return shim.Error("Invalid Signatures")
			}
			proofBytes = secpShaProof.ToBytes()
			stub.PutState("Proof:"+secpShaProof.Name(), proofBytes)
		}

		err = stub.PutState("Proof:"+name, bufferData)
		if err != nil {
			fmt.Printf("Error Saving Proof to Data %s\n", err)
			return shim.Error(fmt.Sprintf("Error Saving Proof to Data %s", err))
		}

		chaincodeEvent.SecpProof = secpProof
		chaincodeEvent.SecpShaProof = secpShaProof

		//return shim.Success(nil)

	default:
		t.logger.Error("Invalid function type")
		return shim.Error("Received unknown function invocation")
	}

	jsonEvent, err2 := json.Marshal(struct {
		Event *kevlarChainCodeEvent `json: Event`
		Type  string                `json: Type`
	}{Event: &chaincodeEvent, Type: function})

	if err2 != nil {
		log.Printf("%s: Could not marshal JSON: %v", function, err2)
	}

	evt := pbg.Event{
		Message: jsonEvent,
		Hooks:   argsProof.Hooks,
		Rules:   argsProof.Rules,
	}

	b, err := proto.Marshal(&evt)
	if err != nil {
		log.Printf("%s: Could not marshal proto: %v", function, err)
	}

	//fmt.Printf("kevlarChainCodeEvent: %v\n", chaincodeEvent)
	//fmt.Printf("jsonEvent: %v\n", jsonEvent)

	s1, _ := prettyprint(jsonEvent)
	fmt.Printf("%s\n", s1)

	key := fmt.Sprintf("skuchain.com/kevlar/%s/0.1.0", function)
	stub.SetEvent(key, b)

	return shim.Success(nil)

}

//  query of a chaincode
func (t *kevlarChainCode) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("No argument specified"))
	}
	name := args[0]
	proofBytes, err := stub.GetState("Proof:" + name)

	if err != nil || len(proofBytes) == 0 {
		return shim.Error(fmt.Sprintf("%s is not found", name))
	}
	secpProof := new(ElementProof.SecP256k1ElementProof)
	secpShaProof := new(ElementProof.SecP256k1SHA2ElementProof)

	err = secpProof.FromBytes(proofBytes)
	if err == nil {
		return shim.Success(secpProof.ToJSON())
	}

	err = secpShaProof.FromBytes(proofBytes)
	if err == nil {
		return shim.Success(secpShaProof.ToJSON())
	}

	return shim.Success(nil)

}

func main() {
	var logger = shim.NewLogger("kevlar")
	logger.SetLevel(shim.LogInfo)
	kcc := kevlarChainCode{
		logger: logger,
	}
	err := shim.Start(&kcc)
	if err != nil {
		logger.Errorf("Error starting chaincode: %s\n", err)
	}
}
