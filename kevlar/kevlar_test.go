package main

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"testing"

	pbg "bitbucket.org/skuchaindev/kevlar/protos"
	"github.com/btcsuite/btcd/btcec"
	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/stretchr/testify/assert"
)

type keyInfo struct {
	privKeyStr string
	pubKeyStr  string
	address    string
	counter    string
}

func generateKeys() (*keyInfo, error) {
	var err error
	keys := new(keyInfo)
	keys.privKeyStr, err = newPrivateKeyString()
	if err != nil {
		fmt.Printf("error generating private key: %v", err.Error())
		return nil, fmt.Errorf("error generating private key: %v", err.Error())
	}
	keys.pubKeyStr, err = newPubKeyString(keys.privKeyStr)
	if err != nil {
		fmt.Printf("error generating public key: %v", err.Error())
		return nil, fmt.Errorf("error generating public key: %v", err.Error())
	}
	keys.address = newAddress(keys.pubKeyStr)
	return keys, nil
}

//generates and returns SHA256 private key string
func newPrivateKeyString() (string, error) {
	privKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return "", fmt.Errorf("Error generating private key\n")
	}
	privKeyBytes := privKey.Serialize()
	privKeyString := hex.EncodeToString(privKeyBytes)
	return privKeyString, nil
}

//generates and returns first forty characters of sha256 hash of public key string
func newAddress(pubKeyStr string) string {
	pubKeyBytes, err := hex.DecodeString(pubKeyStr)
	if err != nil {
		fmt.Printf("error decoding pubkeystring (%s)", pubKeyStr)
	}
	hasher := sha256.New()
	hasher.Write(pubKeyBytes)
	hashedPubKeyBytes := []byte{}
	hashedPubKeyBytes = hasher.Sum(hashedPubKeyBytes)
	hashedPubKeyString := hex.EncodeToString(hashedPubKeyBytes[0:20])
	address := hashedPubKeyString
	return address
}

//generates and returns SHA256 public key string fromessage private key string input
func newPubKeyString(privKeyString string) (string, error) {
	privKeyBytes, err := hex.DecodeString(privKeyString)
	if err != nil {
		return "", fmt.Errorf("error decoding private key string (%s)", privKeyString)
	}
	_, pubKey := btcec.PrivKeyFromBytes(btcec.S256(), privKeyBytes)
	pubKeyBytes := pubKey.SerializeCompressed()
	pubkKeyString := hex.EncodeToString(pubKeyBytes)
	return pubkKeyString, nil
}

func getBytes(function string, args []string) [][]byte {
	bytes := make([][]byte, 0, len(args)+1)
	bytes = append(bytes, []byte(function))
	for _, s := range args {
		bytes = append(bytes, []byte(s))
	}
	return bytes
}

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// func TestInit(t *testing.T) {

// 	t.Parallel()

// 	var logger = shim.NewLogger("kevlar_test")
// 	bst := kevlarChainCode{
// 		logger: logger,
// 	}
// 	stub := shim.NewMockStub("kevlar", &bst)
// 	if stub == nil {
// 		t.Fatalf("MockStub creation failed")
// 	}

// 	resp := stub.MockInit("1", [][]byte{})
// 	assert.Equal(t, 200, int(resp.Status), "Init: Invoke response code not equal")
// }

// func TestInvoke(t *testing.T) {

// 	t.Parallel()

// 	p1 := proofTx.SupercededBy{}
// 	out1, _ := proto.Marshal(&p1)

// 	testCases := []struct {
// 		Name            string
// 		TxID            string
// 		Args            [][]byte
// 		ExpectedCode    int32
// 		ExpectedMessage string
// 	}{
// 		{
// 			Name:            "Missing_args",
// 			TxID:            "1",
// 			Args:            [][]byte{[]byte("anything")},
// 			ExpectedCode:    500,
// 			ExpectedMessage: "Insufficient arguments found\n",
// 		},
// 		{
// 			Name:            "Invalid_format",
// 			TxID:            "2",
// 			Args:            [][]byte{[]byte("invoke"), []byte("A")},
// 			ExpectedCode:    500,
// 			ExpectedMessage: "Invalid argument (A) expected hex\n",
// 		},
// 		{
// 			Name:            "Invalid_proto_def_empty_name",
// 			TxID:            "2",
// 			Args:            [][]byte{[]byte("invoke"), []byte(hex.EncodeToString(out1))},
// 			ExpectedCode:    500,
// 			ExpectedMessage: "Invokes: Invalid argument expected ProofTX protocol buffer ERR:",
// 		},
// 		{
// 			Name:            "Invalid_proto_def",
// 			TxID:            "2",
// 			Args:            [][]byte{[]byte("invoke"), []byte(hex.EncodeToString([]byte("A")))},
// 			ExpectedCode:    500,
// 			ExpectedMessage: "Invalid argument expected ProofTX protocol buffer ERR:(unexpected EOF)\n",
// 		},
// 	}

// 	var logger = shim.NewLogger("kevlar_test")
// 	bst := kevlarChainCode{
// 		logger: logger,
// 	}
// 	stub := shim.NewMockStub("kevlar", &bst)
// 	for _, d := range testCases {
// 		stub.MockTransactionStart(d.TxID)
// 		res := stub.MockInvoke(d.TxID, d.Args)
// 		stub.MockTransactionEnd(d.TxID)
// 		assert.Equal(t, d.ExpectedCode, res.Status, "%s: Invoke response code not equal", d.Name)
// 		assert.Equal(t, d.ExpectedMessage, res.Message, "%s: Invoke response message not equal", d.Name)
// 	}
// }

// func TestInvoke_Query(t *testing.T) {

// 	t.Parallel()

// 	testCases := []struct {
// 		Name            string
// 		TxID            string
// 		Args            [][]byte
// 		ExpectedCode    int32
// 		ExpectedMessage string
// 	}{
// 		{
// 			Name:            "Missing_args",
// 			TxID:            "1",
// 			Args:            [][]byte{[]byte("query")},
// 			ExpectedCode:    500,
// 			ExpectedMessage: "Insufficient arguments found\n",
// 		},
// 		// {
// 		// 	Name:            "Invalid_function",
// 		// 	TxID:            "2",
// 		// 	Args:            [][]byte{[]byte("invoke"), []byte("A"), []byte("B"), []byte("123")},
// 		// 	ExpectedCode:    500,
// 		// 	ExpectedMessage: "Invalid function type (invoke)",
// 		// },
// 	}
// 	var logger = shim.NewLogger("kevlar_test")
// 	bst := kevlarChainCode{
// 		logger: logger,
// 	}
// 	stub := shim.NewMockStub("kevlar", &bst)
// 	for _, d := range testCases {
// 		stub.MockTransactionStart(d.TxID)
// 		res := stub.MockInvoke(d.TxID, d.Args)
// 		stub.MockTransactionEnd(d.TxID)
// 		assert.Equal(t, d.ExpectedCode, res.Status, "%s: Invoke response code not equal", d.Name)
// 		assert.Equal(t, d.ExpectedMessage, res.Message, "%s: Invoke response message not equal", d.Name)
// 	}
// }

func TestInvoke_CreateProof(t *testing.T) {

	t.Parallel()

	keys, err := generateKeys()
	if err != nil {
		t.Fatalf("Could not generate keys")
	}
	creatorPubKey, err := hex.DecodeString(keys.pubKeyStr)
	if err != nil {
		t.Fatalf("Could not generate creator pub key")
	}

	p0 := pbg.ProofTX{
		Type:      pbg.ProofType_SECP256K1,
		Name:      "Exists",
		Threshold: 1,
		Data:      "{\"test\":\"data\"}",
		PubKeys:   [][]byte{creatorPubKey},
		EventData: "{\"test\":\"event data\"}",
	}
	out0, _ := proto.Marshal(&p0)

	p1 := pbg.ProofTX{
		Type:      pbg.ProofType_SECP256K1,
		Name:      "Test1",
		Threshold: 1,
		Data:      "{\"test\":\"data\"}",
		PubKeys:   [][]byte{creatorPubKey},
		EventData: "{\"test\":\"event data\"}",
	}
	out1, _ := proto.Marshal(&p1)

	p2 := pbg.ProofTX{
		Type:      pbg.ProofType_SECP256K1,
		Name:      "Test2",
		Threshold: 2,
		Data:      "{\"test\":\"data\"}",
		PubKeys:   [][]byte{creatorPubKey},
		EventData: "{\"test\":\"event data\"}",
	}
	out2, _ := proto.Marshal(&p2)

	p3 := pbg.ProofTX{
		Type:      pbg.ProofType_SECP256K1,
		Name:      "Test3",
		Threshold: 1,
		Data:      "{\"test\":\"data\"}",
		PubKeys:   [][]byte{[]byte(`AAKEY`)},
		EventData: "{\"test\":\"event data\"}",
	}
	out3, _ := proto.Marshal(&p3)

	// p5 := proofTx.ProofTX{
	// 	Type:      proofTx.ProofTX_SECP256K1SHA2,
	// 	Name:      "Test4",
	// 	Threshold: 1,
	// 	Data:      "{\"test\":\"data\"}",
	// 	PubKeys:   [][]byte{[]byte(`AAKEY`)},
	// 	EventData: "{\"test\":\"event data\"}",
	// }
	// out5, _ := proto.Marshal(&p5)

	// p6 := proofTx.ProofTX{
	// 	Type:      proofTx.ProofTX_SECP256K1SHA2,
	// 	Name:      "Test5",
	// 	Threshold: 1,
	// 	Data:      "{\"test\":\"data\"}",
	// 	Digests:   [][]byte{[]byte(`HELLO`)},
	// 	PubKeys:   [][]byte{creatorPubKey},
	// 	EventData: "{\"test\":\"event data\"}",
	// }
	// out6, _ := proto.Marshal(&p6)

	// bb32, err := generateRandomBytes(32)
	// if err != nil {
	// 	t.Fatalf("Could not generate bytes")
	// }
	// p7 := proofTx.ProofTX{
	// 	Type:      proofTx.ProofTX_SECP256K1SHA2,
	// 	Name:      "Test6",
	// 	Threshold: 1,
	// 	Data:      "{\"test\":\"data\"}",
	// 	Digests:   [][]byte{bb32},
	// 	PubKeys:   [][]byte{creatorPubKey},
	// 	EventData: "{\"test\":\"event data\"}",
	// }
	// out7, _ := proto.Marshal(&p7)

	// p8 := proofTx.ProofTX{
	// 	Type:      5,
	// 	Name:      "Test7",
	// 	Threshold: 1,
	// 	Data:      "{\"test\":\"data\"}",
	// 	Digests:   [][]byte{bb32},
	// 	PubKeys:   [][]byte{creatorPubKey},
	// 	EventData: "{\"test\":\"event data\"}",
	// }
	// out8, _ := proto.Marshal(&p8)

	testCases := []struct {
		Name            string
		TxID            string
		Args            [][]byte
		ExpectedCode    int32
		ExpectedMessage string
	}{
		{
			Name:            "Create_proof_name_exists",
			TxID:            "0",
			Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out0))},
			ExpectedCode:    500,
			ExpectedMessage: "Proof Name:Exists already claimed\n",
		},
		{
			Name:            "Create_proof_success",
			TxID:            "1",
			Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out1))},
			ExpectedCode:    200,
			ExpectedMessage: "",
		},
		{
			Name:            "invalid_threshold",
			TxID:            "2",
			Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out2))},
			ExpectedCode:    500,
			ExpectedMessage: "Invalid Threshold of 2 for 1 keys\n",
		},
		{
			Name:            "Invalid_public_key",
			TxID:            "3",
			Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out3))},
			ExpectedCode:    500,
			ExpectedMessage: "Invalid Public Key: [65 65 75 69 89]",
		},
		// {
		// 	Name:            "invalid_public_key_sha",
		// 	TxID:            "4",
		// 	Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out5))},
		// 	ExpectedCode:    500,
		// 	ExpectedMessage: "Invalid Public Key: [65 65 75 69 89]",
		// },
		// {
		// 	Name:            "invalid_digest_sha",
		// 	TxID:            "5",
		// 	Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out6))},
		// 	ExpectedCode:    500,
		// 	ExpectedMessage: "Invalid Digest Length",
		// },
		// {
		// 	Name:            "correct_digest",
		// 	TxID:            "6",
		// 	Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out7))},
		// 	ExpectedCode:    200,
		// 	ExpectedMessage: "",
		// },
		// {
		// 	Name:            "invalid_proof_type",
		// 	TxID:            "7",
		// 	Args:            [][]byte{[]byte("createProof"), []byte(hex.EncodeToString(out8))},
		// 	ExpectedCode:    500,
		// 	ExpectedMessage: "Invalid Proof Type",
		// },
	}

	var logger = shim.NewLogger("kevlar_test")
	bst := kevlarChainCode{
		logger: logger,
	}
	stub := shim.NewMockStub("kevlar", &bst)

	stub.MockTransactionStart("001")
	err = stub.PutState("Proof:Exists", []byte(`hello`))
	if err != nil {
		t.Fatalf("Could not put state")
	}
	stub.MockTransactionEnd("002")
	for _, d := range testCases {
		stub.MockTransactionStart(d.TxID)
		res := stub.MockInvoke(d.TxID, d.Args)
		stub.MockTransactionEnd(d.TxID)

		assert.Equal(t, d.ExpectedCode, res.Status, "%s: Invoke response code not equal", d.Name)
		assert.Equal(t, d.ExpectedMessage, res.Message, "%s: Invoke response message not equal", d.Name)
	}
}
